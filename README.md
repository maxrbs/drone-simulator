# Drone Simulator
## Description
### RU
Реализация полета дрона на площади парка на Unreal Engine 4.
Имеется вид от лица наблюдателя (TPS) и вид от лица дрона (FPS). При прохождении через кольца появляются эффекты взрывов, при столкновении с объектами – эффекты удара. 
Реализовано управление через клавиатуру + мышь и через контроллер Xbox.

### EN
Implementation of a drone flight in the park square on Unreal Engine 4.
There is a view from the perspective of an observer (TPS) and a view from the perspective of a drone (FPS). When passing through the rings, explosion effects appear, when colliding with objects, impact effects appear.
Implemented control via keyboard + mouse and via the Xbox controller.

## Screenshots
![Screen1](/Sharing/Screenshots/1.png)
![Screen2](/Sharing/Screenshots/2.png)
![Screen3](/Sharing/Screenshots/3.png)
![Screen4](/Sharing/Screenshots/4.png)
![Screen5](/Sharing/Screenshots/5.png)

## Links
Build on itch: https://maxrbs.itch.io/drone-sim